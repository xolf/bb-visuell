import jtoolbox.*;

public class EigenLinie
{
    private EigenAusgabePanel wurzel,folger;
    private Linie linie;
    
    public void setzeWurzel(EigenAusgabePanel wurzelNeu) {
        wurzel = wurzelNeu;
    }
    
    public void setzeFolger(EigenAusgabePanel Neu) {
        folger = Neu;
    }
    
    public void zeichne() {
        if(wurzel != null) {
            if(folger != null) {
                linie = new Linie(wurzel.berechneMitteBreite(), wurzel.erhalteAbstandVonOben(),folger.berechneMitteBreite(), folger.erhalteY());
            }
        }
    }
    
    public void entfernen() {
        if(linie != null) {
            linie.entfernen();
        }
    }
}
