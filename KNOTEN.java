public class KNOTEN extends BAUMELEMENT
{
    private BAUMELEMENT linkerNachfolger;
    private BAUMELEMENT rechterNachfolger;
    private DATENELEMENT daten;
    private int vaterNeu;
    private String wortNeu[][];
    
    public KNOTEN(DATENELEMENT dneu)
    { 
        daten = dneu;
        linkerNachfolger = new Abschluss();
        rechterNachfolger = new Abschluss();
    }
    
    public KNOTEN (DATENELEMENT dneu, BAUMELEMENT lNachfolger, BAUMELEMENT rNachfolger)
    {
        daten = dneu;
        linkerNachfolger = lNachfolger;
        rechterNachfolger = rNachfolger;
    }
    
    public DATENELEMENT Suchen (String suchSchluessel)
    {
        if(daten.SchluesselIstGleich(suchSchluessel))
        {
            return daten;
        }else{
            if(daten.SchluesselIstGroesserAls(suchSchluessel))
            {               
                    return linkerNachfolger.Suchen(suchSchluessel);
            }else{
                    return rechterNachfolger.Suchen(suchSchluessel);
            }
          }
    }
    
    public BAUMELEMENT Einfuegen(DATENELEMENT datenNeu)
    {
        if(daten.IstGleich(datenNeu))
        {
            System.out.println("Die Daten existieren schon!");
        }else {
            if(daten.IstGroesserAls(datenNeu))
            {
                linkerNachfolger = linkerNachfolger.Einfuegen(datenNeu);
                

            }else {
                rechterNachfolger = rechterNachfolger.Einfuegen(datenNeu);
            }
        }
        return this;
   
    }
    
    public void InorderAusgeben()
    {
        linkerNachfolger.InorderAusgeben();
        daten.InformationAusgeben();
        rechterNachfolger.InorderAusgeben();
    }
    
    public void PreorderAusgeben()
    {
        daten.InformationAusgeben();
        linkerNachfolger.PreorderAusgeben();
        rechterNachfolger.PreorderAusgeben();
    }
    
    public void PostorderAusgeben()
    {
        linkerNachfolger.PostorderAusgeben();
        rechterNachfolger.PostorderAusgeben();
        daten.InformationAusgeben();
    }
    
    public void StrukturAusgeben(int tiefe)
    {
        String s= "";
        for (int i= 0; i<tiefe; i++)
        {s = s + "    "; 
        } 
        System.out.println (s+ daten.SchluesselAlsStringGeben());
        linkerNachfolger.StrukturAusgeben(tiefe +1);
        rechterNachfolger.StrukturAusgeben(tiefe +1);
    }
    
    public int zaehlen()
    {
        return rechterNachfolger.zaehlen() + linkerNachfolger.zaehlen() +1;
    }
    
    public void StrukturAuslesen(BBAuslese bba, int tiefe, int vater)
    {
        vaterNeu = bba.Feldlaenge();
        bba.Eintragen(daten.SchluesselAlsStringGeben(), tiefe, vater);
        linkerNachfolger.StrukturAuslesen(bba, tiefe+1,vaterNeu);
        rechterNachfolger.StrukturAuslesen(bba, tiefe+1,vaterNeu);
    }
    
    public int maxTiefe(int tiefe)
    {
        int linkeTiefe = linkerNachfolger.maxTiefe(tiefe+1), rechteTiefe = rechterNachfolger.maxTiefe(tiefe+1);
        if(linkeTiefe > rechteTiefe) {
            return linkeTiefe;
        }
        return rechteTiefe;
    }
 
}
