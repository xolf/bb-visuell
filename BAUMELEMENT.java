public abstract class BAUMELEMENT
{

    abstract DATENELEMENT Suchen(String  suchSchluessel);
    abstract BAUMELEMENT Einfuegen (DATENELEMENT datenNeu);
    abstract void InorderAusgeben();
    abstract void PreorderAusgeben();
    abstract void PostorderAusgeben();
    abstract void StrukturAusgeben(int tiefe);
    abstract int zaehlen();
    abstract int maxTiefe(int tiefe);
    abstract void StrukturAuslesen(BBAuslese bba, int tiefe, int vater);
}
