
interface DATENELEMENT
{
    void InformationAusgeben();
    boolean SchluesselIstGleich(String vergleichswert);
    boolean IstKleinerAls(DATENELEMENT vergleich);
    boolean IstGroesserAls(DATENELEMENT vergleich);
    boolean IstGleich(DATENELEMENT vergleich);
    boolean SchluesselIstGroesserAls(String vergleich);
    String SchluesselAlsStringGeben();
    
}

