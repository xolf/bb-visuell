import jtoolbox.*;

public class EigenAusgabePanel
{
    private RechteckMitRundenEcken panel;
    private Ausgabe text;
    
    private int radius = 5, SchriftGroesse = 12, BuchstabenPlatz, innereBreite, innereHoehe, breite, hoehe, x, y;
    private String inhalt;
    
    public EigenAusgabePanel(String inhaltNeu, int xNeu, int yNeu, int breiteNeu, int hoeheNeu)
    {
        inhalt = inhaltNeu;
        x = xNeu;
        y = yNeu;
        breite = breiteNeu;
        hoehe = hoeheNeu;
        innereBreite = breite-10;
        innereHoehe = hoehe-10;
        
        berechneBuchstabenPlatz();
        
        panel = new RechteckMitRundenEcken(x, y, breite, hoehe, radius);
        text = new Ausgabe(inhalt, x, y, breite, hoehe);
        // Zentriert den Inhalt
        text.setzeAusrichtung(1);
        
        // Wenn der Text größer ist als die gegebene Box
        while(innereHoehe < SchriftGroesse) {
            SchriftGroesse--;
        }
        // Wenn der Platz für den Text nicht reicht, wird die Schriftgröße verkleinert
        while(BuchstabenPlatz < inhalt.length()) {
            SchriftGroesse--;
            berechneBuchstabenPlatz();
        }
        
        if(SchriftGroesse < 1) {
            SchriftGroesse = 1;
        }
        text.setzeSchriftgroesse(SchriftGroesse);
        text.setzeSchriftName("Monospaced");
    }
    
    public void entfernen()
    {
        panel.entfernen();
        text.entfernen();
    }
    
    public int berechneBuchstabenPlatz()
    {
        return BuchstabenPlatz = innereBreite/SchriftGroesse;
    }
    
    public int berechneMitteBreite()
    {
        return (int) x+(breite/2);
    }
    
    public int berechneMitteHoehe()
    {
        return (int) y+(hoehe/2);
    }
    
    
    public int erhalteY()
    {
        return y;
    }
    
    public int erhalteAbstandVonOben()
    {
        return y+hoehe;
    }
}
