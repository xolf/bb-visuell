public class WOERTERBUCHEINTRAG implements DATENELEMENT
{
   private String wort;
   private String bedeutung;
   
   WOERTERBUCHEINTRAG (String wneu, String bneu)
   {
       wort = wneu;
       bedeutung = bneu;
    }
   
   public String BedeutungGeben()
   {
       return bedeutung;
    }
    
   public void BedeutungSetzen(String bneu)
   {
      bedeutung = bneu;  
    }
    
   public void InformationAusgeben()
   {
       System.out.println(wort + " - " + bedeutung);
    }
    
   public String WortGeben()
   {
       return wort;
    }
    
   public boolean IstKleinerAls(DATENELEMENT vergleich)
   {
       WOERTERBUCHEINTRAG eintrag = (WOERTERBUCHEINTRAG) vergleich;
       return (wort.compareTo(eintrag.WortGeben()) < 0);
    }
    
    public boolean SchluesselIstGleich(String vergleich)
    {
        return wort == vergleich;
    }
    
    public boolean IstGleich(DATENELEMENT vergleich)
    {
       WOERTERBUCHEINTRAG eintrag = (WOERTERBUCHEINTRAG) vergleich;
       return (wort.compareTo(eintrag.WortGeben()) == 0);
    }
    
    public boolean IstGroesserAls(DATENELEMENT vergleich)
    {
       WOERTERBUCHEINTRAG eintrag = (WOERTERBUCHEINTRAG) vergleich;
       return (wort.compareTo(eintrag.WortGeben()) > 0);
    }
    
    public boolean SchluesselIstGroesserAls(String vergleich)
    {
        return (wort.compareTo(WortGeben()) > 0);
    }
    
    public String SchluesselAlsStringGeben()
    {
        return  wort;
    }
    
    }

