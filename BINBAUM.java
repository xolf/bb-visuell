
public class BINBAUM
{
    private BAUMELEMENT wurzel;
   

    
    public BINBAUM()
    {
        wurzel = new Abschluss();
    }
    
    public DATENELEMENT Suchen (String suchSchluessel)
    {
       return wurzel.Suchen(suchSchluessel);
        }
    
    public void Einfuegen(DATENELEMENT datenNeu)
    {
        wurzel = wurzel.Einfuegen(datenNeu);
    }
    
    public void InorderAusgeben()
    {
        wurzel.InorderAusgeben();
    }
    
    public void StrukturAusgeben()
    {
        wurzel.StrukturAusgeben(0);
    }
    
    public void StrukturAuslesen(BBAuslese bba)
    {
        wurzel.StrukturAuslesen(bba, 0, -1);
    }
    

    public void PreorderAusgeben()
    {
        wurzel.PreorderAusgeben();
    }
    
    public void PostorderAusgeben()
    {
        wurzel.PostorderAusgeben();
    }
    
    public int AnzahlZaehlen()
    {
        return wurzel.zaehlen();
    }
    
    public int maxTiefe()
    {
        return wurzel.maxTiefe(0);
    }
    
}
