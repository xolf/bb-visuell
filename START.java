import jtoolbox.*;
/**
 * Write a description of class START here.
 * 
 * @author (Niclas Heun) 
 * @version (0,1)
 */

public class START implements ITuWas
{
    private EigenAusgabePanel[] panels;
    private EigenLinie[] linien;
    private Taste t;
    private D_Eingabe dlg1, dlg2;
    private BINBAUM bb;
    private BBAuslese bba;
    
    public START()
    {
        t = new Taste("Einfuegen", 10,10,200,50);
        //flo ist ein idiot
        
 
        dlg1 = new D_Eingabe();
        dlg1.setzeMeldungstext("Wort");
        dlg1.setzeTitel("Neuer Wörterbucheintrag");
        dlg2 = new D_Eingabe();
        dlg2.setzeMeldungstext("Bedeutung");
        dlg2.setzeTitel("Neuer Wörterbucheintrag");
        t.setzeLink(this);
        bb = new BINBAUM();
        bba = new BBAuslese();
        
        zeichne();
    }
    
    public void tuWas(int ID)
    {
        dlg1.zeigeMeldung();
        dlg2.zeigeMeldung();
        WOERTERBUCHEINTRAG neuerWoerterbucheintrag;
        neuerWoerterbucheintrag = new WOERTERBUCHEINTRAG(dlg1.leseErgebnis(), dlg2.leseErgebnis());
        bb.Einfuegen(neuerWoerterbucheintrag);
        
        zeichne();
    }
    
    public void manuellEinfuegen(WOERTERBUCHEINTRAG neuerWoerterbucheintrag)
    {
        bb.Einfuegen(neuerWoerterbucheintrag);
        zeichne();
    }
    
    public void zeichne()
    {
        AusgabeEntfernen();
        bba.FeldSetzen(bb.AnzahlZaehlen());
        bb.StrukturAuslesen(bba);
        String[][] bbaFeld = bba.FeldGeben();
        panels = new EigenAusgabePanel[bba.Feldlaenge()];
        linien = new EigenLinie[bba.Feldlaenge()];
        int maxTiefe = bb.maxTiefe();
        EigenAusgabePanel ursprung = null;
        for(int i=0; i<bba.Feldlaenge();i++) {
            int tiefe = Integer.parseInt(bbaFeld[i][1]);

            if(i > 0) {
                linien[i] = new EigenLinie();
                int vater = Integer.parseInt(bbaFeld[i][2]);
                int vaterMitte = panels[vater].berechneMitteBreite();
                int panelBreite = 100;
                
                if(bbaFeld[i][0].compareTo(bbaFeld[vater][0])>0)
                {
                    // Rechts
                    panels[i] = new EigenAusgabePanel(bbaFeld[i][0], vaterMitte+ dynamischeSpreizung(maxTiefe, tiefe), 70*(tiefe+1), panelBreite, 30);
                }else{
                    // Links
                    panels[i] = new EigenAusgabePanel(bbaFeld[i][0], vaterMitte-panelBreite-dynamischeSpreizung(maxTiefe, tiefe), 70*(tiefe+1), panelBreite, 30);
                }
                
                linien[i].setzeFolger(panels[i]);
                linien[i].setzeWurzel(panels[vater]);
                linien[i].zeichne();
            }else{
                panels[i] = new EigenAusgabePanel(bbaFeld[i][0], 120*maxTiefe, 70*(tiefe+1), 80, 30);
            }
            
            
            //System.out.println(bbaFeld[i][0]);
        }
        
        //panels[0] = new EigenAusgabePanel("Wurzel", 100, 100, 80, 30);
        //panels[1] = new EigenAusgabePanel("Links", 50, 200, 80, 30);
        //linien[0] = new EigenLinie();
        //linien[0].setzeWurzel(panels[0]);
        //linien[0].setzeLinkerFolger(panels[1]);
        //linien[0].zeichne();
    }
    
    
    public void AusgabeEntfernen()
    {
        for(int i=0; i<bba.Feldlaenge();i++)
        {
            if(panels[i] != null) panels[i].entfernen();
            if(linien[i] != null) linien[i].entfernen();
        }
    }
    
    private int maxElementeProTiefe(int tiefe) {
        if(tiefe == 0) {
            return 1;
        }
        return (int)(0.5*(2^tiefe));
    }
    
    private int dynamischeSpreizung(int maxTiefe, int tiefe)
    {
        int Spreizung = 50;
        if(maxTiefe-1 == tiefe)
        {
            return 0;
        }else{
            for(int i= maxTiefe-1; i>tiefe; i--)
            {
                Spreizung = Spreizung *2;
                System.out.println(Spreizung);
            }
            return Spreizung;
        }
        //int Spreizung = (maxTiefe-tiefe-1)*100;
        //System.out.println(Spreizung);
        
        
        
        //int untereTiefe = maxElementeProTiefe(maxTiefe)*100;
        //int versetzung = maxElementeProTiefe(tiefe)*100;
        //return untereTiefe/versetzung;
        //int spreizung = 1/x*100+((y)*5)^2;
        //System.out.println("Y: "+y+", X:"+x+", Spreizung:"+spreizung);
        //return spreizung;
    }
    
}
