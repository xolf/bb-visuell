
/**
 * Englisch-deutsches W&ouml;rterbuch als geordneter Bin&auml;rbaum (ohne Composite Pattern) implementiert.
 * 
 * @author (Peter Brichzin) 
 * @version (20.3.09)
 */
public class WOERTERBUCH
{
    /**
     * Implementierung des W&ouml;rterbuchs als geordneter Bin&auml;rbaum 
     */
     //private BINBAUM woerterbuch;
     private START start;
    /**
     * Konstruktor f�r Objekte der Klasse WOERTERBUCH, erzeugt ein englischen W&ouml;rterbuch 
     * mit 7 Eintr&auml;gen, wie in Abbildung 6 bzw. 8 von Kapitel 7.
     */
    public WOERTERBUCH()
    {
        //woerterbuch = new BINBAUM();
        start = new START();
        Einfuegen("clip","Klammer, abschneiden, anstecken" );
        Einfuegen("car","Auto, Fahrzeug, Waggon" );
        Einfuegen("call","Aanrufen, telefonieren" );
        Einfuegen("cat","Katze" );
        Einfuegen("care","Fuersorge, Sorgfalt" );
        Einfuegen("cave","Hoehle, aushoehlen, einbrechen" );
        Einfuegen("crab","Krabbe, Krebs, Griesgram" );
        Einfuegen("coin","Muenze, auspraegen, erfinden" );
        Einfuegen("cook","kochen" );
        Einfuegen("cube","Würfel" );
        Einfuegen("crow","wachsen" );
        
    }

     /**
     * Einfuegen erzeugt ein ein Objekt der Klasse WOERTERBUCHEINTRAG mit den Eingabewerten und
     * f&uuml;gt diesen W�rterbucheintrag sortiert in die Liste woerterbuch ein. Es wird verhindert, 
     * dass ein englisches Wort mehrfach im W&ouml;rterbuch abgespeichert werden kann.
     * @param wort englisches Wort
     * @param bedeutung die deutsche(n) Bedeutung(en).
     */
    public void Einfuegen(String wort, String bedeutung)
    {
        WOERTERBUCHEINTRAG neuerWoerterbucheintrag;
        neuerWoerterbucheintrag = new WOERTERBUCHEINTRAG(wort, bedeutung);
        start.manuellEinfuegen(neuerWoerterbucheintrag);
    }
    
     /**
     * Sucht ein englisches Wort im W&ouml;rterbuch
     * @param vergleichswert gesuchter Schl&uuml;ssel
     * @return gesuchter W&ouml;rterbucheintrag
     */
    /*public DATENELEMENT Suchen(String gesuchtesWort)
    {
         return woerterbuch.Suchen(gesuchtesWort);
    }
    
 
    
    /**
    * Die Methode BedeutungSetzen erm&ouml;glicht es, bei einem englischen Wort
    * die deutsche Bedeutung zu ver&auml;ndern.
    * @param gesuchtesWort englisches Wort, dessen Bedeutung ver&auml;ndert werden soll
    * @param bedeutungNeu neue Bedeutung
    
    public void BedeutungSetzen(String gesuchtesWort, String bedeutungNeu)
    {
        WOERTERBUCHEINTRAG woerterbucheintrag;
        woerterbucheintrag = (WOERTERBUCHEINTRAG) Suchen(gesuchtesWort);
        if (woerterbucheintrag == null)
        {
            System.out.println("Eintrag existiert nicht!");
        }
        else
        {
            woerterbucheintrag.BedeutungSetzen(bedeutungNeu);
        }
        
    }
    
    public void InorderAusgeben()
    {
        woerterbuch.InorderAusgeben();
        
    }
    public void PreorderAusgeben()
    {
        woerterbuch.PreorderAusgeben();
        
    }
    public void PostorderAusgeben()
    {
        woerterbuch.PostorderAusgeben();
        
    }
    
    public void StrukturAusgeben()
    {
        woerterbuch.StrukturAusgeben();
    }
    
    public int AnzahlZaehlen()
    {
        return woerterbuch.AnzahlZaehlen();
    }
    
    public void zeichne()
    {
        start.zeichne();
    }
    */
}
